<?php


class BankAccount implements IfaceBankAccount
{

    private $balance = null;
    // private $account = null;

    public function __construct(Money $openingBalance)
    {
        $this->balance = $openingBalance;
    }

    public function balance()
    {
        return $this->balance;
    }

    public function deposit(Money $amount)
    {
        //implement this method
        $amt = (int) (string) $amount;
        $bal = (int) (string) $this->balance;

        //update balance of my bank acc
        $this->balance = $bal+$amt;
    }

    public function transfer(Money $amount, BankAccount $account)
    {
        //implement this method
        $amt = (int) (string) $amount;
        $bal = (int) (string) $this->balance;



        if($amt > $bal) {
            throw new Exception("Withdrawl amount larger than balance");
        }


        $this->balance = $bal-$amt;

        if($bal == $this->balance) { 
            throw new Exception("Target account balance remains same after failed transfer");
        }

        // ur account
        $acc_bal = $account->balance;

        $account->balance += $amt;

        if($acc_bal == $account->balance) {
            throw new Exception("Destination account balance remains same after failed transfer");
        }
        
    }

    public function withdraw(Money $amount)
    {
        //implement this method
        $amt = (int) (string) $amount;
        $bal = (int) (string) $this->balance;

        // throw exception if withdrawal amt is larger than bal
        if($amt > $bal) {
            throw new Exception("Withdrawl amount larger than balance");
        }

        //update balance of ur bank acc
        $this->balance = $bal-$amt;
    }
}